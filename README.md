Preprocessing the images consists of bias field correction, and 
masking out non-relevant regions, such that the images consist of
only normal appearing white-matter, or hypo/hyper-intensities of
interest.

The two inputs are 

1. A single preprocessed T1 or T2 MRI, and
2. A partial (at least 500 voxels) segmentation of WMH corresponding to the image in 1). 

Images should be in .nii (NIfTI) format, and have the same spatial dimensions.

This script outputs probability maps and binarized segmentations for each iteration.                                                                                    

Number of iterations and voxels per sample (which is also the minimum number
of required voxels to initialize the model) is configurable.
The model used can be changed also, via the caret package, to something
other than random forest if desired. (See code comments for details)